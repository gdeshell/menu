/**
 * Toggle the menu of the Cosmos desktop environment.
 */
[DBus (name = "com.cosmos.menu")]
interface Menu : Object
{
	public abstract void toggle() throws GLib.Error;
}

void main()
{
	Menu menu = Bus.get_proxy_sync (BusType.SESSION, "com.cosmos.menu", "/com/cosmos/menu");
	menu.toggle();
}
