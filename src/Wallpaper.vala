using Gtk;

/**
  * Dbus of cosmos-wallpaper
  */
[DBus (name = "com.cosmos.wallpaper")]
private interface CosmosWallpaper: Object {

	// Get the type of wallpaper
	public abstract WallpaperType get_wallpaper_type() throws Error;
	// Get the timestamp of the wallpaper (used for video)
	public abstract int64 get_timestamp () throws Error;
	// Get the path of the wallpaper 
	public abstract string get_path () throws Error;

	public abstract signal void wallpaper_changed (WallpaperType type, string path);


	/**
	* type of wallpaper if it is picture, video or gif
	*/
	public enum WallpaperType {
		NULL = 0,
		PICTURE = 1,
		VIDEO = 2,
		GIF = 3
	}
}


public class Wallpaper : Gtk.DrawingArea {

	Gtk.Picture? picture = null;
	CosmosWallpaper wallpaper;

	construct {
		vexpand = true;
		hexpand = true;
	}

	/**
	* Constructor of the wallpaper
	* @param size: size of the content of Wallpaper widget (width, height) 
	*/
	public Wallpaper (Vec2i size) {
		base.content_width = size.x;
		base.content_height = size.y;

		try {
			wallpaper = Bus.get_proxy_sync (BusType.SESSION, "com.cosmos.wallpaper", "/com/cosmos/wallpaper");
			var path = wallpaper.get_path();
			var type = wallpaper.get_wallpaper_type();
			init_picture (path, type);

			wallpaper.wallpaper_changed.connect ((type, path) => {
				init_picture (path, type);
				queue_draw();
			});
		}
		catch (Error e) {
			error (e.message);
		}


	}


	private 	void init_picture (string path, CosmosWallpaper.WallpaperType type) {
		try {
			/**
			 * Create a picture from the path of the wallpaper
			 * depending on the type of the wallpaper
			 */
			switch (type) {

				case CosmosWallpaper.WallpaperType.PICTURE:
					picture = new Gtk.Picture.for_filename (path);
					break;

				case CosmosWallpaper.WallpaperType.VIDEO:
					var media = Gtk.MediaFile.for_filename (path);
					media.set_playing (true);
					media.set_loop (true);
					media.set_muted (true);
					media.update(wallpaper.get_timestamp());
					picture = new Gtk.Picture.for_paintable(media);
					break;

				case CosmosWallpaper.WallpaperType.GIF:
					var paintable = new CosmosDesktop.Gif (path);
					picture = new Gtk.Picture.for_paintable (paintable);
					break;

				default:
					warning ("Unknown wallpaper type (Error DBus) unrecheable code");
					break;
			}
		}
		catch (Error e) {
			warning(e.message);
		}
	}

	/**
	* Draw the wallpaper
	*
	* add a blur effect to the wallpaper
	* @param snapshot: snapshot of the wallpaper
	*/
	public override void snapshot(Gtk.Snapshot snapshot) {
		Graphene.Rect screen;
		screen = {{0, 0}, {base.content_width, base.content_height}};

		// If the picture is null, draw a black background
		if (picture == null) {
			snapshot.append_color({0.1f, 0.1f, 0.1f, 1.0f}, screen);
			return;
		}

		// Get the paintable of the picture
		unowned Gdk.Paintable texture;
		texture = picture.get_paintable();

		// draw a black background with a blur effect 
		snapshot.append_color({0f, 0f, 0f, 1.0f}, screen);
		snapshot.push_blur(25f);
		snapshot.append_texture(texture as Gdk.Texture, screen);
		snapshot.pop();

		// Add a black filter to the wallpaper
		snapshot.append_color({0f, 0f, 0f, 0.5f}, screen);
    }
}
