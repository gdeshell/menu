using Gtk;

[DBus (name = "com.cosmos.compositor")]
public interface Compositor : Object
{
	public abstract void expo_toggle() throws GLib.Error;
	public abstract void scale_toggle () throws Error;

}

public class WindowFull : Adw.ApplicationWindow {

	private Gtk.Overlay	_overlay = new Gtk.Overlay();
	private Menu		_menu = new Menu();
	private Vec2i		_screen;
	private Wallpaper	_wallpaper;

	[DBus (name = "com.cosmos.menu")]
	public class Service : Object
	{
		public void toggle() throws GLib.Error {
			if (self.visible == true) {
				compositor.expo_toggle();
			}
			self.visible = !self.visible;
		}
	}

	private static void run_dbus () {
		Bus.own_name (BusType.SESSION, "com.cosmos.menu",
			BusNameOwnerFlags.NONE,
			on_bus_aquired,
			() => {printerr("[Dbus] (com.cosmos.menu) is running \n");},
			() => printerr("[Dbus] Could not acquire name (com.cosmos.menu)\n"));
	}	
	
	public static void on_bus_aquired (DBusConnection conn) {
		try {
			var service = new Service ();
			conn.register_object ("/com/cosmos/menu", service);
		} catch (IOError e) {
			stderr.printf ("Could not register service: %s\n", e.message);
		}
	}

	public static WindowFull self;
	public static Compositor compositor;

	construct {
		self = this;
		decorated = false;
	}

	public WindowFull () {
		init();

		compositor = Bus.get_proxy_sync (BusType.SESSION, "com.cosmos.compositor", "/com/cosmos/compositor");
		run_dbus();

		base.set_size_request(_screen.x, _screen.y);

		_menu = new Menu();
		content = _overlay;
		_overlay.add_overlay(_wallpaper);
		_overlay.add_overlay(_menu);

		// Events
		Gtk.EventControllerKey keyboard = new Gtk.EventControllerKey();
		((Gtk.Widget)base).add_controller(keyboard);
		keyboard.key_pressed.connect((key, state) => {
			if (key == Gdk.Key.Escape) {
				base.visible = false;
				return true;
			}
			return false;
		});

		_menu.onOpenApp.connect(() => {
			base.visible = false;
		});
	}

	private void init() {
		_screen = get_screen_size();
		_wallpaper = new Wallpaper(_screen);
	}

	private Vec2i get_screen_size() {
		var monitor = Gdk.Display.get_default().get_monitors().get_item(0) as Gdk.Monitor;
		unowned var geometry = monitor.geometry;
		return ({geometry.width, geometry.height});
	}
}
