using Gtk;

public class Card : Gtk.Button {
	Gtk.Box box = new Gtk.Box(Gtk.Orientation.VERTICAL, 5);
	Gtk.Image image = new Gtk.Image();
	Gtk.Label title;
	public AppInfo app_info {get; private set;}

	construct {
		has_frame = false;
		child = box;
	}

	public signal void onOpenApp();

	public Card (AppInfo info) {
		app_info = info;
		title = new Gtk.Label(info.get_name ()) {
			max_width_chars = 16,
			ellipsize = Pango.EllipsizeMode.END
		};
		image.set_from_gicon (info.get_icon ());
		image.set_pixel_size(100);
		box.append(image);
		box.append(title);

		base.clicked.connect (()=>{
			info.launch (null, null);
			onOpenApp();
		});
	}

}

public class Page : Gtk.Grid {
	
	public int x = 0;
	public int y = 0;
	public bool is_full = false;

	construct {
		row_spacing = 70;
		column_spacing = 80;
		halign = Gtk.Align.CENTER;
		valign = Gtk.Align.CENTER;
		margin_start = 50;
		margin_end = 50;
		margin_bottom = 50;
		hexpand = true;
		vexpand = true;
		name = "menu-page";
	}

	public bool add (Card widget) {
		if (is_full)
		{
			return false;
		}

		attach (widget, x, y, 1, 1);
		x++;

		if (x == 6)
		{
			x = 0;
			y++;
		}
		if (y == 3)
		{
			y = 0;
			is_full = true;
		}
		return true;
	}
}

public class Menu : Gtk.Box {

	Gtk.Entry search = new Gtk.Entry(){width_request=500, height_request=135, halign = Align.CENTER, margin_top = 20};
	Adw.Carousel? carousel = null;
	Adw.CarouselIndicatorDots? indicator = null;

	construct {
		orientation = Gtk.Orientation.VERTICAL;
		name = "menu";
		spacing = 0;
	}


	public void fill_the_menu (Card []cards, string search_text = "") {
		init_carousel ();
		var page = new Page ();

		foreach (unowned var info in cards) {
			if (info.app_info.should_show () == false || "Avahi" in info.get_name ())
				continue;

			if ((search_text in info.app_info.get_name ().ascii_down()) == false && (search_text in info.app_info.get_description ().ascii_down()) == false)
				continue;

			if (!page.add (info)) {
				carousel.append (page);
				page = new Page ();
				page.add (info);
			}
		}
		carousel.append (page);
	}

	void init_carousel () {
		if (carousel != null)
			base.remove (carousel);
		if (indicator != null)
			base.remove (indicator);
		carousel = new Adw.Carousel () {
			hexpand = true,
			vexpand = true
		}; 
		indicator = new Adw.CarouselIndicatorDots () {
			halign = Gtk.Align.CENTER,
			hexpand = true,
			vexpand = false,
			margin_bottom = 50,
		};
		indicator.set_carousel (carousel);
		base.append (search);
		base.append (carousel);
		base.append (indicator);
	}

	public signal void onOpenApp();

	Card []cards = {};
	public Menu () {
		
		search.placeholder_text = "Type to search ...";


		var all_info = AppInfo.get_all ();
		foreach (unowned var info in all_info) {
			if (info.should_show () == false || "Avahi" in info.get_name ())
				continue;
			var card = new Card (info);
			card.onOpenApp.connect (()=> {
				this.onOpenApp();
				search.text = "";
				fill_the_menu (cards, "");
			});
			cards += card;
		}

		fill_the_menu (cards);

		search.has_frame = false;
		search.buffer.notify["text"].connect(()=> {
			fill_the_menu (cards, search.text);
		});

		carousel.visible = true;
	}
}
