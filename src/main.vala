public class ExampleApp : Adw.Application {
	private WindowFull window;

	construct {
		application_id= "com.cosmos.menu";
	}

	public override void activate () {
		window = new WindowFull () {
			application = base
		};

		init_gtk_layer_shell ();


		// CSS Styling
		var provider = new Gtk.CssProvider ();
		provider.load_from_data (css_string.data);
		Gtk.StyleContext.add_provider_for_display (Gdk.Display.get_default (), provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

		window.present ();
		window.visible = false;
	}


	private void init_gtk_layer_shell () {
		GtkLayerShell.init_for_window(window);
		GtkLayerShell.set_namespace (window, "cosmos-menu");
		
		GtkLayerShell.set_margin(window, GtkLayerShell.Edge.TOP, -500);
		GtkLayerShell.set_margin(window, GtkLayerShell.Edge.BOTTOM, -500);
		GtkLayerShell.set_margin(window, GtkLayerShell.Edge.LEFT, -500);
		GtkLayerShell.set_margin(window, GtkLayerShell.Edge.RIGHT, -500);

		// GtkLayerShell.set_anchor (window, GtkLayerShell.Edge.TOP, false);
		GtkLayerShell.set_exclusive_zone (window, -1);

		GtkLayerShell.set_keyboard_mode (window, GtkLayerShell.KeyboardMode.EXCLUSIVE);
		GtkLayerShell.set_layer (window, GtkLayerShell.Layer.TOP);
	}

	public static int main (string[] args) {
		var app = new ExampleApp ();
		return app.run (args);
	}
}

const string css_string = """
#menu button{
	background-color:rgba(128, 128, 128, 0);
	padding-left:15px;
	padding-right:15px;
	padding-top:5px;
	padding-bottom:5px;
	border-radius:15px;
	color:white;
}

#menu button:hover{
	background-color:rgba(210, 210, 210, 0.30);
}

#menu entry {
	margin:50px;
	border-radius:15px;
	background-color:rgba(255, 255, 255, 0.90);
}

#menu carouselindicatordots {
	border-radius:15px;
}
""";
